# Use a pipeline as a high-level helper
from transformers import pipeline
import torch

transcriber = pipeline(task="automatic-speech-recognition", model="openai/whisper-small",device=torch.device("mps"))
transcriber.model.to("mps")
t = transcriber("~/Downloads/mlk.flac")
print(t)
