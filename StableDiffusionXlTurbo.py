from diffusers import  DiffusionPipeline
import torch
import gradio as gr


def generate(pPrompt,pNegPrompt=""):
    pipe = DiffusionPipeline.from_pretrained("stabilityai/sdxl-turbo", torch_dtype=torch.float16, variant="fp16",add_watermarker=False)
    pipe.to("mps")
    
    image = pipe(
        prompt=pPrompt,
        negative_prompt=pNegPrompt,
        num_inference_steps=25, 
        guidance_scale=7.0,
        ).images[0]
    return image


demo = gr.Interface(
    title="première app IA",
    allow_flagging=None,
    fn=generate,
    inputs=[gr.Textbox(label="prompt",lines=2, placeholder="Prompt..."),
            gr.Textbox(label="negative prompt",lines=2, placeholder="Negative Prompt...")
            ],
    outputs="image",
)
demo.launch()